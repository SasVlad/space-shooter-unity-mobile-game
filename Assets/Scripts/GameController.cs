﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
    public GameObject restartButton;
    public Text gameOverText;

    private bool gameOver;
    private bool restart;
    private int score;
	// Use this for initialization
	void Start () {
        gameOver = false;
        restart = false;
        restartButton.SetActive(false);
        //restartButton.GetComponentInChildren<Text>().text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
       StartCoroutine(SpawnWaves());
	}
    //void Update()
    //{
    //    if (restart)
    //    {
    //        if (Input.GetKeyDown(KeyCode.R))
    //        {
    //            SceneManager.LoadScene(Application.loadedLevel);
    //        }
    //    }
    //}
    private IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true) {
            for (int i = 0; i < 10; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
            if (gameOver)
            {
                restartButton.SetActive(true);
                //restartButton.GetComponentInChildren<Text>().text = "Press 'R' for restart";
                restart = true;
                break;
            }
        }
    }
    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }
    // Update is called once per frame
    void UpdateScore () {
        scoreText.text = "Score: " + score;
	}
    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(Application.loadedLevel);
    }
}
