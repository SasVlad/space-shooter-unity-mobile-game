﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContract : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject!=null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        else
        {
            Debug.Log("Cannot find 'GameController' script in gameObjects");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boundary")||other.CompareTag("Enemy"))
        {
            return;
        }
        Instantiate(explosion,transform.position,transform.rotation);
        if (other.CompareTag("Player"))
        {          
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }
        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
